<?php

namespace App;

/**
 * Class Admin
 *
 * @package App
 */
class Admin extends Human implements Spam
{
    private string $role = 'admin';

    public function getData():string
    {
        return $this->name . ' ' . $this->email;
    }

    public function recoverData():string
    {
        return $this->name . ' ' . $this->email;
    }

    public function addUser():string
    {
        $role = 'user';

        return $this->id . $role;
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function deleteUser($id):string
    {
        return 'пользователь c id' . $id . 'удален';
    }

    public function addOrganizer():string
    {
        return 'добавлен организатор с ид=' . $this->id;
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function deleteOrganizer($id):string
    {
        return 'организатор c id' . $id . 'удален';
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function block($id):string
    {
        return 'пользователь с id' . $id . 'заблокирован';
    }
}
