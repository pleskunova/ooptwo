<?php

namespace App;

/**
 * Class Human
 *
 * @package App
 */
abstract class Human
{
    public string $name;
    public string $email;
    public int $id;

    /**
     * Human constructor.
     *
     * @param $id
     * @param $name
     * @param $email
     */
    public function __construct($id, $name, $email)
    {
        $this->id    = $id;
        $this->name  = $name;
        $this->email = $email;
    }

    abstract public function getData();

    abstract public function recoverData();
}
