<?php

namespace App;

/**
 * Class Organizer
 *
 * @package App
 */
class Organizer extends Human implements Spam
{
    public string $events;
    private string $role = 'organizer';

    /**
     * Human constructor.
     *
     * @param $id
     * @param $name
     * @param $email
     */
    public function __construct($id, $name, $email, $events)
    {
        parent::__construct($id, $name, $email);
        $this->events = $events;
    }

    public function getData():string
    {
        return $this->name . ' ' . $this->email . ' ' . $this->role;
    }

    public function recoverData():string
    {
        return $this->name . ' ' . $this->email;
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function block($id):string
    {
        return 'пользователь с id' . $id . 'заблокирован';
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function invite($id):string
    {
        //where user_id = $id;
        return 'отправлено письмо с приглашением на мероприятие' . $this->events . 'от отправителя' . $this->email;
    }
}
