<?php

namespace App;

/**
 * Interface Spam
 *
 * @package App
 */
interface Spam
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function block($id): string;
}
