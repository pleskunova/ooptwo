<?php

use App\Admin;
use App\Organizer;
use App\User;

require __DIR__ . '/vendor/autoload.php';

$user = new User('1', 'sveta', 'sv');
echo $user->getData() . '<br>';
echo $user->recoverData() . '<br>';

$admin = new Admin('2', 'admin', 'admin');
echo $admin->getData() . '<br>';
echo $admin->recoverData() . '<br>';
echo $admin->addUser() . '<br>';
echo $admin->addOrganizer() . '<br>';
echo $admin->deleteUser('1') . '<br>';
echo $admin->deleteOrganizer('3') . '<br>';
echo $admin->block('1');

$organizer = new Organizer('3', 'organizer', 'org', 'test');
echo $organizer->getData() . '<br>';
echo $organizer->recoverData() . '<br>';
echo $organizer->block('1') . '<br>';
echo $organizer->invite('1') . '<br>';
